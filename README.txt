This repo contains my codes for Computer Graphics assignments - http://www.cs.colostate.edu/~cs410/yr2015fa/home_assignments.php.

Update: The development for this has been moved to https://github.com/prashant-mehta/ray-tracer.